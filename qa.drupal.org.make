core = 6.x
api = 2
projects[drupal][version] = "6.38"


; Probably mostly PressFlow patches.
projects[drupal][patch][] = "https://bitbucket.org/drupalorg-infrastructure/qa.drupal.org/raw/6.x-prod/patches/drupal.diff"


; Modules
projects[admin_menu][version] = "1.8"
projects[adminrole][version] = "1.3"

projects[bakery][version] = "2.x-dev"
projects[bakery][download][revision] = "e4665c7"
; https://www.drupal.org/node/1450842: Remove rebaking the cookie on subsites, which removes the master switch.
projects[bakery][patch][] = "https://www.drupal.org/files/issues/1450842-bakery-sso-cookie-master-5-d6.patch"

projects[chart][version] = "1.3"
projects[coder][version] = "2.x-dev"
projects[coder][download][revision] = "daea522"
projects[coder_tough_love][version] = "1.1"
projects[drupalorg_crosssite][version] = "3.x-dev"
projects[drupalorg_crosssite][download][revision] = "17a2a5d"
projects[logintoboggan][version] = "1.10"
projects[markdown][version] = "1.3"
projects[module_filter][version] = "1.7"
projects[paranoia][version] = "1.1"
projects[pathauto][version] = "1.5"
projects[phpass][version] = "2.1"
projects[project_issue_file_review][version] = "2.x-dev"
projects[project_issue_file_review][download][revision] = "c6b507f"
projects[rules][version] = "1.4"
projects[security_review][version] = "1.x-dev"
projects[security_review][download][revision] = "8bf4535"
projects[tabs][version] = "1.3"
projects[token][version] = "1.15"
projects[views][version] = "2.18"
projects[views_bulk_operations][version] = "1.17"
projects[views_datasource][version] = "1.0-beta2"
